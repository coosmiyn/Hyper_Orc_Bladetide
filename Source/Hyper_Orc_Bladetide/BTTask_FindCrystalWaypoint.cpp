// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_FindCrystalWaypoint.h"
#include "GoblinAI.h"
#include "BotWaypoint.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"
#include "Engine.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"

EBTNodeResult::Type UBTTask_FindCrystalWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AGoblinAI* Controller = Cast<AGoblinAI>(OwnerComp.GetAIOwner());

	if (Controller == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	ABotWaypoint* Waypoint = Controller->GetWaypoint();
	AActor* NewWaypoint = nullptr;

	TArray<AActor*> AllWaypoints;
	TArray<AActor*> CrystalWaypoints;

	// If the AI has no target, it gets the CrystalWaypoint as a target and moves towards it
	// otherwise it will go to the crystal.
	// This way, the AI can go for a different waypoint first and also use different paths.
	if (OwnerComp.GetBlackboardComponent()->GetValueAsObject(BlackboardKey.SelectedKeyName) != nullptr)
	{
		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(BlackboardKey.GetSelectedKeyID(), OwnerComp.GetBlackboardComponent()->GetValueAsObject(TEXT("Crystal")));
		return EBTNodeResult::Succeeded;
	}
	else
	{
		UGameplayStatics::GetAllActorsOfClass(Controller, ABotWaypoint::StaticClass(), AllWaypoints);

		if (AllWaypoints.Num() == 0)
			return EBTNodeResult::Failed;

		for (int i = 0; i < AllWaypoints.Num(); i++)
		{
			if (AllWaypoints[i]->GetName().Contains(TEXT("CrystalWaypoint")))
				CrystalWaypoints.Add(AllWaypoints[i]);
		}

		NewWaypoint = CrystalWaypoints[FMath::RandRange(0, CrystalWaypoints.Num() - 1)];

		if (NewWaypoint)
		{
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(BlackboardKey.GetSelectedKeyID(), NewWaypoint);
			return EBTNodeResult::Succeeded;
		}

		return EBTNodeResult::Failed;
	}
}