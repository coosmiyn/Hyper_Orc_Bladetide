// Fill out your copyright notice in the Description page of Project Settings.


#include "TrapSaw.h"

void ATrapSaw::BeginPlay()
{
	Super::BeginPlay();

	SkeletalMesh->PlayAnimation(Animation, true);
}

void ATrapSaw::ActivateTrap(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		LastActivateTime = GetWorld()->GetTimeSeconds();

		FPointDamageEvent DmgEvent;
		DmgEvent.DamageTypeClass = TrapDamageType;
		DmgEvent.Damage = Damage;
		Goblin->TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

		DmgEvent.Damage = Damage;
		TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

		SimulateActiveTrap();
	}
}

void ATrapSaw::SimulateActiveTrap()
{

}