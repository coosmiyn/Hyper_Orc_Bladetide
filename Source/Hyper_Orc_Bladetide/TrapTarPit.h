// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTrap.h"
#include "TrapTarPit.generated.h"

/**
 * 
 */
UCLASS()
class HYPER_ORC_BLADETIDE_API ATrapTarPit : public ABaseTrap
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float SlowRate = 0.5f;

protected:
		void ActivateTrap(AActor* OtherActor) override;

		void StepOff(AActor* OtherActor) override;
	
};
