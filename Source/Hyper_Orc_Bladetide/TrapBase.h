// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "TrapBase.generated.h"

UCLASS()
class HYPER_ORC_BLADETIDE_API ATrapBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATrapBase();
	ATrapBase(const FObjectInitializer& ObjectInitializer);

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* MeshComponent;*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
		UBoxComponent* BoxComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
		UBoxComponent* RootBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats)
		float Damage = 35;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats)
		float Health = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		float TrapDamage = 10.0f;


protected:
	virtual void Init();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Attacking")
		TSubclassOf<UDamageType> TrapDamageType;

	UFUNCTION()
		virtual void OnStepOn(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnStepOff(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual void ActivateTrap(class AActor* OtherActor);

	UFUNCTION()
		virtual void SimulateActiveTrap();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
};
