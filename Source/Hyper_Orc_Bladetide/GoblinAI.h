// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AIPerceptionComponent.h"
#include "Goblin.h"
#include "BotWaypoint.h"
#include "MainCharacter.h"
#include "Crystal.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "GoblinAI.generated.h"

UCLASS()
class HYPER_ORC_BLADETIDE_API AGoblinAI : public AAIController
{
	GENERATED_BODY()

public:

	AGoblinAI();

protected:

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual void OnUnPossess() override;

	virtual FRotator GetControlRotation() const override;

	UFUNCTION()
		void OnPawnDetected(const TArray<AActor*>& DetectedPawns);

	UFUNCTION()
		void OnTargetDetected(AActor* Actor, FAIStimulus const stimulus);

public:

	// Crystal
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		ACrystal* Crystal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		ABotWaypoint* FinalWaypoint;

	// AI Components
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		UBehaviorTreeComponent* BehaviorComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		UBlackboardComponent* BlackboardComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		class UAISenseConfig_Sight* Sight;

	// Sight variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float AISightRadius = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float AISightAge = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float AISightLoseRadius = AISightRadius + 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float AIFieldOfView = 180.0f;

	// Blackboard keys
	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName TargetEnemyKeyName;

	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName PatrolLocationKeyName;

	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName WaypointKeyName;

	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName HasLineOfSightKeyName;

	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName CrystalKeyName;

	UPROPERTY(EditDefaultsOnly, Category = AI)
		FName LastEnemyLocationKeyName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = AI)
		FName RemembersPlayerKeyName;	

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = AI)
		FName CrystlWaypointKeyName;

	// Blackboard keys get/set methods
	bool GetLineOfSight();

	void SetLineOfSight(bool value);

	ABotWaypoint* GetWaypoint();

	AMainCharacter* GetTargetEnemy();

	void SetTargetEnemy(APawn* NewTarget);

	ACrystal* GetCrystal();

	void SetCrystal(AActor* Target);

	ABotWaypoint* GetCrystalWaypoint();

	void SetCrystalWaypoint(AActor* Target);

	void SetWaypoint(AActor* Target);

	FVector GetLastEnemyLocation();

	void SetLastEnemyLocation(FVector location);

	bool GetRemembersPlayer();

	void SetRemembersPlayer(bool value);

	FORCEINLINE UBehaviorTreeComponent* GetBehaviourComp() const { return BehaviorComp; }

	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
};
