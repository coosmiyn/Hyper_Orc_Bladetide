// Fill out your copyright notice in the Description page of Project Settings.


#include "FirePitTrap.h"
#include "Goblin.h"

// Sets default values
AFirePitTrap::AFirePitTrap()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	/*MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("meshComponent"));
	MeshComponent->SetMobility(EComponentMobility::Movable);
	MeshComponent->SetupAttachment(RootBox);*/

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);
	TrapActivateCooldown = 0.5f;
}

// Called when the game starts or when spawned
void AFirePitTrap::BeginPlay()
{
	Super::BeginPlay();

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AFirePitTrap::OnStepOn);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &AFirePitTrap::OnStepOff);
}

void AFirePitTrap::OnStepOn(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->ApplyDoT(10.0f);
	}
}

void AFirePitTrap::OnStepOff(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	/*AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->OnLeaveTrap();
	}*/
	StepOff(OtherActor);
}

void AFirePitTrap::ActivateTrap(AActor* OtherActor)
{

}

void AFirePitTrap::OnRetriggerTrap()
{

}

void AFirePitTrap::SimulateActiveTrap()
{

}

void AFirePitTrap::StepOff(AActor* OtherActor)
{
}

// Called every frame
void AFirePitTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

