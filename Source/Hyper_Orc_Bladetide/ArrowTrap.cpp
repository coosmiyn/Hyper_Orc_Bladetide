// Fill out your copyright notice in the Description page of Project Settings.


#include "ArrowTrap.h"
#include "Engine.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Goblin.h"

// Sets default values
AArrowTrap::AArrowTrap()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	/*MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("meshComponent"));
	MeshComponent->SetMobility(EComponentMobility::Movable);
	MeshComponent->SetupAttachment(RootBox);*/

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);
	TrapActivateCooldown = 0.5f;
}

// Called when the game starts or when spawned
void AArrowTrap::BeginPlay()
{
	Super::BeginPlay();

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AArrowTrap::OnStepOn);
}

void AArrowTrap::OnStepOn(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		int id = FMath::RandRange(-100, 100);
		ActivateTrap(Goblin);
		GetWorldTimerManager().SetTimer(TimerHandle_ActivateTrap, this, &AArrowTrap::OnRetriggerTrap, TrapActivateCooldown, true);
	}

}

void AArrowTrap::ActivateTrap(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		LastActivateTime = GetWorld()->GetTimeSeconds();

		FPointDamageEvent DmgEvent;
		DmgEvent.DamageTypeClass = PunchDamageType;
		DmgEvent.Damage = Damage;
		Goblin->TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

		DmgEvent.Damage = 0;
		TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

		SimulateActiveTrap();
	}
}

void AArrowTrap::OnRetriggerTrap()
{
	TArray<AActor*> Overlaps;
	int id = FMath::RandRange(-100, 100);

	// Check if any actors are in the attack range
	BoxComponent->GetOverlappingActors(Overlaps, AGoblin::StaticClass());
	for (int32 i = 0; i < Overlaps.Num(); i++)
	{
		AGoblin* OverlappingPawn = Cast<AGoblin>(Overlaps[i]);
		if (OverlappingPawn)
		{
			ActivateTrap(OverlappingPawn);
			//break; /* Uncomment to only attack one pawn maximum */
		}
	}

	//GEngine->AddOnScreenDebugMessage(id, 3.0f, FColor::Red, FString::FromInt(Overlaps.Num()));
	/* No pawns in range, cancel the retrigger timer */
	if (Overlaps.Num() == 0)
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
}

void AArrowTrap::SimulateActiveTrap()
{
}

// Called every frame
void AArrowTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AArrowTrap::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AArrowTrap::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	return 0.0f;
}

