// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Animation/AnimSequence.h"
#include "Goblin.h"
#include "Engine.h"
#include "BaseTrap.generated.h"

UCLASS()
class HYPER_ORC_BLADETIDE_API ABaseTrap : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABaseTrap();

	// Components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
		UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
		UBoxComponent* RootBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		UAnimSequence* Animation;

	// Stats
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float Damage = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float SelfDamage = 10;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float Health = 100;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Attack")
		TSubclassOf<UDamageType> TrapDamageType;

	UFUNCTION()
		virtual void OnStepOn(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnStepOff(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual void ActivateTrap(class AActor* OtherActor);

	void OnRetriggerTrap();

	UFUNCTION()
		virtual void SimulateActiveTrap();

	UFUNCTION()
		virtual void StepOff(AActor* OtherActor);

	FTimerHandle TimerHandle_ActivateTrap;

	float TrapActivateCooldown = 1.0f;

	float LastActivateTime;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

};
