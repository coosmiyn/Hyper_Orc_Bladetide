// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "SpikeTrap.generated.h"

/**
 * 
 */
UCLASS()
class HYPER_ORC_BLADETIDE_API ASpikeTrap : public APawn
{
	GENERATED_BODY()

public:
	ASpikeTrap();
	ASpikeTrap(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
		UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision)
		UBoxComponent* RootBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats)
		float Damage = 25;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Stats)
		float Health = 100;


protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Attacking")
		TSubclassOf<UDamageType> PunchDamageType;

	UFUNCTION()
		virtual void OnStepOn(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual void ActivateTrap(class AActor* OtherActor);

	void OnRetriggerTrap();

	UFUNCTION()
		void SimulateActiveTrap();

	/* Timer handle to manage continous melee attacks while in range of a player */
	FTimerHandle TimerHandle_ActivateTrap;

	TArray<AActor*> ActorsOnTrap;

	float TrapActivateCooldown;

	float LastActivateTime;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

};
