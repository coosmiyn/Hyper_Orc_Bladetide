// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTrap.h"
#include "TrapSaw.generated.h"

/**
 * 
 */
UCLASS()
class HYPER_ORC_BLADETIDE_API ATrapSaw : public ABaseTrap
{
	GENERATED_BODY()

protected:

	float Damage = 10.0f;

	void BeginPlay() override;

	void ActivateTrap(AActor* OtherActor) override;

	void SimulateActiveTrap() override;
};
