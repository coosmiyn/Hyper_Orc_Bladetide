// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"
#include "TrapArrow.h"
#include "TrapFireBall.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Disable the rotation cotrols for the character mesh so it is used only by the camera
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	// Allow the character to be rotated
	GetCharacterMovement()->bOrientRotationToMovement = false;

	// Set the movement variables
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 450.0f;
	GetCharacterMovement()->AirControl = 0.5f;

	// Create the Arm Component which will hold the camera and attach it to the player
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);

	// Set the length of the arm and allow it to rotate
	CameraBoom->TargetArmLength = 50.0f;
	CameraBoom->bUsePawnControlRotation = true;

	// Create the camera for the player and attach it to the Spring Arm component
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);

	// Disable rotation for the camera since it is attached to the Spring Arm component which will rotate.
	FollowCamera->bUsePawnControlRotation = false;

	// Create the projectile and set it's location relative spawn to the player
	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	//FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));
	FP_MuzzleLocation->SetupAttachment(CameraBoom);

	// Set the location where the the gun will be placed (not used at the moment);
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	RaycastLocation = CreateDefaultSubobject<USceneComponent>(TEXT("RaycastLocation"));
	RaycastLocation->SetupAttachment(RootComponent);

	// Create a first person mesh component for the owning player.
	FPSMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	FPSMesh->SetupAttachment(CameraBoom);

}

void AMainCharacter::GivePoints(float value)
{
	Points += value;
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
}


// TO DO: Clamp nicely
void AMainCharacter::ChangeTrap(float Value)
{
	if (Value == 1)
	{
		if (TrapPick == Traps.Num() - 1)
			TrapPick = 0;
		else
			TrapPick++;
	}
	else if (Value == -1)
	{
		if (TrapPick == 0)
			TrapPick = Traps.Num() - 1;
		else
			TrapPick--;
	}

	// Display points and what trap is selected
	GEngine->AddOnScreenDebugMessage(455, 1.0f, FColor::Orange, FString::FromInt(Points));
	GEngine->AddOnScreenDebugMessage(454, 1.0f, FColor::Orange, Traps[TrapPick]->GetName());
}

float AMainCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	Health -= Damage;

	// TO DO: Create a die method
	if (Health <= 0)
	{
		// Disable the collision detectors
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);

		USkeletalMeshComponent* skeletal = GetMesh();
		if (skeletal)
		{
			// Change the physics and collision for the mesh to create a "death" effect
			skeletal->SetCollisionProfileName(TEXT("Ragdoll")); 
			skeletal->SetAllBodiesSimulatePhysics(true);
			skeletal->SetSimulatePhysics(true);
			skeletal->WakeAllRigidBodies();
			skeletal->bBlendPhysics = true;
			
			// Detach the camera and invalidate control imputs
			CameraBoom->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
			CameraBoom->bUsePawnControlRotation = false;
			CameraBoom->SetupAttachment(skeletal);
		}
	}

	return Health;
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);

	PlayerInputComponent->BindAction("PickUpObject", IE_Pressed, this, &AMainCharacter::PickUpObject);
	PlayerInputComponent->BindAction("SpawnBaseTrap", IE_Pressed, this, &AMainCharacter::SpawnBaseTrap);

	PlayerInputComponent->BindAxis("MoveObjectX", this, &AMainCharacter::MoveObject);
	PlayerInputComponent->BindAxis("MoveObjectY", this, &AMainCharacter::MoveObject);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMainCharacter::OnStartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AMainCharacter::OnStopFire);

	PlayerInputComponent->BindAxis("ChangeTrap", this, &AMainCharacter::ChangeTrap);

}

void AMainCharacter::MoveForward(float Value)
{
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void AMainCharacter::MoveRight(float Value)
{
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}

AActor* AMainCharacter::Raycast()
{
	FHitResult OutHit;

	FVector Start = FollowCamera->GetComponentLocation();
	FVector ForwardVector = FollowCamera->GetForwardVector();

	//Start = Start + (ForwardVector * CameraBoom->TargetArmLength);
	Start = Start + ForwardVector;
	FVector End = Start + (ForwardVector * 2000.0f);

	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);

	bool isHit = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams);

	if (isHit)
		return OutHit.GetActor();
	else
		return nullptr;
}

AActor* AMainCharacter::RaycastLookForTile()
{
	FHitResult OutHit;

	FVector Start = FollowCamera->GetComponentLocation();
	FVector ForwardVector = FollowCamera->GetForwardVector();

	//Start = Start + (ForwardVector * CameraBoom->TargetArmLength);
	Start = Start + ForwardVector;
	FVector End = Start + (ForwardVector * 2000.0f);

	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this);

	bool isHit = GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_WorldStatic, CollisionParams);

	if (isHit)
		return OutHit.GetActor();
	else
		return nullptr;
}

void AMainCharacter::OnStartFire()
{
	//GetWorld()->GetTimerManager().ClearTimer(TimerHandle_Fire);

	Fire();
	bCanFire = false;

	GetWorldTimerManager().SetTimer(TimerHandle_Fire, this, &AMainCharacter::Fire, FireCooldown, true);
	
}

void AMainCharacter::OnStopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_Fire);

}

void AMainCharacter::Fire()
{
	if (ProjectileClass != NULL && bCanFire)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			const FRotator SpawnRotation = GetControlRotation();

			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// spawn the projectile at the muzzle
			World->SpawnActor<AProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			GetWorldTimerManager().SetTimer(TimerHandle_CanFire, this, &AMainCharacter::InvalidateFire, FireCooldown, false);
			FPSMesh->PlayAnimation(Animation, false);
		}
	}

}

void AMainCharacter::InvalidateFire()
{
	bCanFire = true;
}

void AMainCharacter::SpawnBaseTrap()
{

	UWorld* World = GetWorld();
	if (World && HeldComponent == nullptr && Points >= TrapCost)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;

		FRotator Rotator;
		FVector SpawnLocation = this->GetActorLocation();

		ABaseTrap* Spawned = World->SpawnActor<ABaseTrap>(Traps[TrapPick], SpawnParams);
		if (Spawned != nullptr)
		{
			Spawned->Tags.Add(TEXT("Movable"));
			HeldComponent = Spawned;
			isPickingObject = true;
			Points -= TrapCost;
		}
	}
}

void AMainCharacter::PickUpObject()
{
	isPickingObject = !isPickingObject;

	// Check if the player is holding anything at the moment
	if (HeldComponent == nullptr)
	{
		// Get the object the player is aiming at
		AActor* PickableObject = Raycast();

		// Check if the Raycast returned something and if it is a "movable" object
		// TO DO: Add class for pickable objecets and check for class instead of tag
		if (PickableObject != nullptr && PickableObject->ActorHasTag("Movable"))
		{
			isPickingObject = true;
			HeldComponent = PickableObject;

			// Store the initial position in case the object is put in an invalid location
			HeldComponentInitialPosition = PickableObject->GetActorLocation();
		}
	}
	else
	{
		isPickingObject = false;
		HeldComponent = nullptr;
	}
}

void AMainCharacter::MoveObject(float Value)
{
	if (HeldComponent != nullptr)
	{
		AActor* SeenObject = RaycastLookForTile();
		if (SeenObject != nullptr)
		{
			// Check if the seen object can store an object/trap
			// TO DO: Add a class for tiles and check the class not the tag
			AStaticMeshActor* Tile = Cast<AStaticMeshActor>(SeenObject);
			if (Tile && SeenObject->GetName().Contains("Cube"))
			{
				// TO DO: make wall traps placeble only on walls
				if (HeldComponent->StaticClass() == ATrapArrow::StaticClass() || HeldComponent->StaticClass() == ATrapFireBall::StaticClass())
				{
					//GEngine->AddOnScreenDebugMessage(36, 3.0f, FColor::Turquoise, TEXT("cube place"));
					if (SeenObject->ActorHasTag("WallPlace"))
					{
						//GEngine->AddOnScreenDebugMessage(36, 3.0f, FColor::Turquoise, TEXT("Wall place"));
						FVector Size = Tile->GetStaticMeshComponent()->Bounds.GetBox().GetSize();
						FVector location = SeenObject->GetActorLocation();
						FRotator rotation = SeenObject->GetActorRotation();
						FVector Origin;
						FVector Bounds;
						SeenObject->GetActorBounds(false, Origin, Bounds);
						//location.Y += Size.Y / 2;
						HeldComponent->SetActorLocation(location);
						HeldComponent->SetActorRotation(rotation);
					}
				}
				if (HeldComponent->StaticClass() != ATrapArrow::StaticClass() || HeldComponent->StaticClass() != ATrapFireBall::StaticClass())
				{
					//GEngine->AddOnScreenDebugMessage(36, 3.0f, FColor::Turquoise, TEXT("Wall place"));
					FVector Size = Tile->GetStaticMeshComponent()->Bounds.GetBox().GetSize();
					FVector location = SeenObject->GetActorLocation();
					FRotator rotation = SeenObject->GetActorRotation();
					FVector Origin;
					FVector Bounds;
					SeenObject->GetActorBounds(false, Origin, Bounds);
					//location.Y += Size.Y / 2;
					HeldComponent->SetActorLocation(location);
					HeldComponent->SetActorRotation(rotation);
				}
			}
			else
			{
				HeldComponent->SetActorLocation(HeldComponentInitialPosition);
			}
		}
	}
}

