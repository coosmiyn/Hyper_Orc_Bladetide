// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/EngineTypes.h"
#include "Projectile.h"
#include "GameFramework/Actor.h"
#include "TrapBase.h"
#include "SpikeTrap.h"
#include "Animation/AnimSequence.h"
#include "BaseTrap.h"
#include "Engine.h"
#include "GameFramework/SpringArmComponent.h"
#include "MainCharacter.generated.h"

UCLASS()
class HYPER_ORC_BLADETIDE_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	// Character class components
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		UCameraComponent* FollowCamera;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* FP_MuzzleLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Raycast)
		class USceneComponent* RaycastLocation;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class AProjectile> ProjectileClass;

	// First-person mesh (arms), visible only to the owning player.
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* FPSMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		FVector GunOffset;



	// Stats
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
	float Health = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Traps)
		TSubclassOf<ABaseTrap> BasicTrap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Traps)
		TArray<TSubclassOf<ABaseTrap>> Traps;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float Points = 100;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
	float FireCooldown = 0.35f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
	float CanFireCooldown = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Stats)
		float TrapCost = 50.0f;

	UPROPERTY(EditAnywhere, BlueprintReadONly, Category = Animation)
		UAnimSequence* Animation;

	void GivePoints(float value);

	int TrapPick = 0;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void ChangeTrap(float Value);

	// Trap placement components
	bool isPickingObject = false;
	AActor* HeldComponent;
	FVector HeldComponentInitialPosition;

	FTimerHandle TimerHandle_Fire;
	FTimerHandle TimerHandle_CanFire;

	bool bCanFire = true;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Character movement
	UFUNCTION()
		void MoveForward(float Value);
	UFUNCTION()
		void MoveRight(float Value);

	UFUNCTION()
		void OnStartFire();

	UFUNCTION()
		void OnStopFire();

	UFUNCTION()
		void InvalidateFire();

	void Fire();

	// Trap placement methods.
	UFUNCTION()
		void MoveObject(float Value);

	UFUNCTION()
		void SpawnBaseTrap();
	UFUNCTION()
		void PickUpObject();


protected:
	AActor* Raycast();

	AActor* RaycastLookForTile();

public:
	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

};
