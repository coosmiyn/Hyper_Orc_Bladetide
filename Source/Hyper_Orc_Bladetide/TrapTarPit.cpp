// Fill out your copyright notice in the Description page of Project Settings.


#include "TrapTarPit.h"

void ATrapTarPit::ActivateTrap(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->DecreaseMoveSpeed(SlowRate);
	}
}

void ATrapTarPit::StepOff(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->DecreaseMoveSpeed(1 / SlowRate);
	}
}