// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseTrap.h"

// Sets default values
ABaseTrap::ABaseTrap()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootComponent = RootBox;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetupAttachment(RootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ABaseTrap::BeginPlay()
{
	Super::BeginPlay();

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ABaseTrap::OnStepOn);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &ABaseTrap::OnStepOff);
	
}

void ABaseTrap::OnStepOn(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		ActivateTrap(Goblin);
		GetWorldTimerManager().SetTimer(TimerHandle_ActivateTrap, this, &ABaseTrap::OnRetriggerTrap, TrapActivateCooldown, true);
	}
}

void ABaseTrap::OnStepOff(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	StepOff(OtherActor);
}

void ABaseTrap::ActivateTrap(AActor* OtherActor)
{
	SimulateActiveTrap();
}

void ABaseTrap::OnRetriggerTrap()
{
	TArray<AActor*> Overlaps;

	// Check if any actors are in the attack range
	BoxComponent->GetOverlappingActors(Overlaps, AGoblin::StaticClass());
	for (int32 i = 0; i < Overlaps.Num(); i++)
	{
		AGoblin* OverlappingPawn = Cast<AGoblin>(Overlaps[i]);
		if (OverlappingPawn)
		{
			ActivateTrap(OverlappingPawn);
			//break; /* Uncomment to only attack one pawn maximum */
		}
	}

	/* No pawns in range, cancel the retrigger timer */
	if (Overlaps.Num() == 0)
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
}

void ABaseTrap::SimulateActiveTrap()
{
	if (SkeletalMesh)
	{
		if (Animation)
		{
			SkeletalMesh->PlayAnimation(Animation, false);
		}
	}
}

void ABaseTrap::StepOff(AActor* OtherActor)
{
}

// Called every frame
void ABaseTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseTrap::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ABaseTrap::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Health -= DamageAmount;

	if (Health <= 0)
	{
		Destroy(true, true);
	}

	return DamageAmount;
}

