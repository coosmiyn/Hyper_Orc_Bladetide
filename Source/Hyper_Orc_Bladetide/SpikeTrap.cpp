// Fill out your copyright notice in the Description page of Project Settings.


#include "SpikeTrap.h"
#include "Goblin.h"
#include "Engine.h"

ASpikeTrap::ASpikeTrap()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);

	TrapActivateCooldown = 0.5f;
}

ASpikeTrap::ASpikeTrap(const FObjectInitializer& ObjectInitializer)
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);

	TrapActivateCooldown = 0.5f;
}

void ASpikeTrap::BeginPlay()
{
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ASpikeTrap::OnStepOn);
}

void ASpikeTrap::OnStepOn(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		int id = FMath::RandRange(-100, 100);
		ActivateTrap(Goblin);
		GetWorldTimerManager().SetTimer(TimerHandle_ActivateTrap, this, &ASpikeTrap::OnRetriggerTrap, TrapActivateCooldown, true);
	}
}

void ASpikeTrap::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void ASpikeTrap::ActivateTrap(class AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		LastActivateTime = GetWorld()->GetTimeSeconds();

		FPointDamageEvent DmgEvent;
		DmgEvent.DamageTypeClass = PunchDamageType;
		DmgEvent.Damage = Damage;
		Goblin->TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

		DmgEvent.Damage = 0;
		TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

		SimulateActiveTrap();
	}
}

void ASpikeTrap::OnRetriggerTrap()
{
	TArray<AActor*> Overlaps;
	int id = FMath::RandRange(-100, 100);

	// Check if any actors are in the attack range
	BoxComponent->GetOverlappingActors(Overlaps, AGoblin::StaticClass());
	for (int32 i = 0; i < Overlaps.Num(); i++)
	{
		AGoblin* OverlappingPawn = Cast<AGoblin>(Overlaps[i]);
		if (OverlappingPawn)
		{
			ActivateTrap(OverlappingPawn);
			//break; /* Uncomment to only attack one pawn maximum */
		}
	}

	//GEngine->AddOnScreenDebugMessage(id, 3.0f, FColor::Red, FString::FromInt(Overlaps.Num()));
	/* No pawns in range, cancel the retrigger timer */
	if (Overlaps.Num() == 0)
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
}

void ASpikeTrap::SimulateActiveTrap()
{
}

void ASpikeTrap::Tick(float DeltaTime)
{
}

void ASpikeTrap::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
}

float ASpikeTrap::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Health -= DamageAmount;

	//GEngine->AddOnScreenDebugMessage(-32, 3.0f, FColor::Red, FString::FromInt(Health));

	if (Health <= 0)
	{
		Destroy(true, true);
	}

	return DamageAmount;
}
