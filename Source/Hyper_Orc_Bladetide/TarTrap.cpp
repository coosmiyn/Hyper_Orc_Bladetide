// Fill out your copyright notice in the Description page of Project Settings.


#include "TarTrap.h"
#include "Goblin.h"
#include "Engine.h"

// Sets default values
ATarTrap::ATarTrap()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);
}

// Called when the game starts or when spawned
void ATarTrap::BeginPlay()
{
	Super::BeginPlay();
	
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATarTrap::OnStepOn);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &ATarTrap::OnStepOff);
}

void ATarTrap::OnStepOn(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//GEngine->AddOnScreenDebugMessage(-3, 5.0f, FColor::Blue, TEXT("StepOn"));
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->DecreaseMoveSpeed(0.5f);
	}
}

void ATarTrap::OnStepOff(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//GEngine->AddOnScreenDebugMessage(-3, 5.0f, FColor::Blue, TEXT("StepOn"));
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->DecreaseMoveSpeed(1 / 0.5f);
	}
}

void ATarTrap::ActivateTrap(AActor* OtherActor)
{
}

// Called every frame
void ATarTrap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATarTrap::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ATarTrap::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	return 0.0f;
}

