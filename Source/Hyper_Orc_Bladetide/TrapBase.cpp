// Fill out your copyright notice in the Description page of Project Settings.


#include "TrapBase.h"
#include "Engine.h"
#include "Goblin.h"

// Sets default values
ATrapBase::ATrapBase()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	/*MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("meshComponent"));
	MeshComponent->SetMobility(EComponentMobility::Movable);
	MeshComponent->SetupAttachment(RootBox);*/

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATrapBase::OnStepOn);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);

}

ATrapBase::ATrapBase(const FObjectInitializer& ObjectInitializer)
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	/*MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("meshComponent"));
	MeshComponent->SetMobility(EComponentMobility::Movable);
	MeshComponent->SetupAttachment(RootBox);*/

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATrapBase::OnStepOn);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);

}

void ATrapBase::Init()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBox"));
	RootBox->SetMobility(EComponentMobility::Movable);
	RootComponent = RootBox;

	/*MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("meshComponent"));
	MeshComponent->SetMobility(EComponentMobility::Movable);
	MeshComponent->SetupAttachment(RootBox);*/

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	BoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATrapBase::OnStepOn);
	BoxComponent->SetMobility(EComponentMobility::Movable);
	BoxComponent->SetupAttachment(RootBox);
}

// Called when the game starts or when spawned
void ATrapBase::BeginPlay()
{
	Super::BeginPlay();

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATrapBase::OnStepOn);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &ATrapBase::OnStepOff);
	
}

void ATrapBase::OnStepOn(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ActivateTrap(OtherActor);
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Health -= 10.0f;

		if (Health <= 0)
		{
			Destroy();
		}

		//Goblin->TakeDamage(DamageEvent.Damage, DamageEvent, NULL, this);
	}
}

void ATrapBase::OnStepOff(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void ATrapBase::ActivateTrap(class AActor* OtherActor)
{
}

void ATrapBase::SimulateActiveTrap()
{
	
}

// Called every frame
void ATrapBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATrapBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ATrapBase::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	return 0.0f;
}
