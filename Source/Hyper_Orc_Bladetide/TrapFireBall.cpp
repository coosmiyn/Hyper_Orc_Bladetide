// Fill out your copyright notice in the Description page of Project Settings.


#include "TrapFireBall.h"

void ATrapFireBall::ActivateTrap(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->ApplyDoT(10.0f);
	}
}

void ATrapFireBall::StepOff(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->OnLeaveTrap();
	}
}
