// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTrap.h"
#include "TrapFireBall.generated.h"

/**
 * 
 */
UCLASS()
class HYPER_ORC_BLADETIDE_API ATrapFireBall : public ABaseTrap
{
	GENERATED_BODY()

protected:
	float Damage = 10.0f;

	void ActivateTrap(AActor* OtherActor) override;

	void StepOff(AActor* OtherActor) override;
};
