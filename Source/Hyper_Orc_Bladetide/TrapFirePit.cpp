// Fill out your copyright notice in the Description page of Project Settings.


#include "TrapFirePit.h"

void ATrapFirePit::ActivateTrap(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->ApplyDoT(10.0f);
	}
}

void ATrapFirePit::StepOff(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		Goblin->OnLeaveTrap();
	}
}
