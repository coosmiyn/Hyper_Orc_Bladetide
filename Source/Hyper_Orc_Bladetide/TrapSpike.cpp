// Fill out your copyright notice in the Description page of Project Settings.


#include "TrapSpike.h"

void ATrapSpike::ActivateTrap(AActor* OtherActor)
{
	AGoblin* Goblin = Cast<AGoblin>(OtherActor);
	if (Goblin)
	{
		LastActivateTime = GetWorld()->GetTimeSeconds();

		FPointDamageEvent DmgEvent;
		DmgEvent.DamageTypeClass = TrapDamageType;
		DmgEvent.Damage = Damage;
		Goblin->TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

		DmgEvent.Damage = Damage;

		SimulateActiveTrap();
		TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);

	}
}

